/*#############################################################################/
	COE718 Project	Fall/2015
	Media Player
	by: Ionathan Zauritz - 500417417
*#############################################################################*/
#include <stdio.h>
#include "LPC17xx.h"
#include "cmsis_os.h"
#include "RTL.H"
#include "LED.h"
#include "GLCD.h"
#include "KBD.h"
#include "usbdmain.h"

#define __FI        1                      /* Font index 16x24               */
#define __USE_LCD   0										/* Uncomment to use the LCD */

#define ITM_Port8(n)    (*((volatile unsigned char *)(0xE0000000+4*n)))
#define ITM_Port16(n)   (*((volatile unsigned short*)(0xE0000000+4*n)))
#define ITM_Port32(n)   (*((volatile unsigned long *)(0xE0000000+4*n)))

#define DEMCR           (*((volatile unsigned long *)(0xE000EDFC)))
#define TRCENA          0x01000000

struct __FILE { int handle; /* Add whatever needed */ };
FILE __stdout;
FILE __stdin;

int fputc(int ch, FILE *f) {
  if (DEMCR & TRCENA) {
    while (ITM_Port32(0) == 0);
    ITM_Port8(0) = ch;
  }
  return(ch);
}

#define mediaPlayerTitle "    Media Centre    "
#define musicTitle "  Stream Music      "
#define imagesTitle "  View Images       "
#define gameTitle "  Play Lazer Dodge  "

#define menuSel 0
#define musicSel 1
#define imagesSel 2
#define gameSel 3

int selected = musicSel;
int mainMenuSel, mainMenuSelNew;
int lastPageSel;

void select();
void mainMenu();
void music();
void images();
void game();

extern unsigned char gotPic[];
extern unsigned char minionsPic[];
extern unsigned char greenPic[];
extern unsigned char r2Pic[];
extern unsigned char lazerPic[];
extern unsigned char gameOverPic[];


//custom delay (in ms)
void delay(long msLen){ 
	long k, count = 0;
	msLen = (msLen - 300) * 16664;
	for(k = 0; k < msLen; k++){
					count++;
			}
}

//LED
void ledAllOff(){
	int i;
	for(i=0; i<8; i++){
		LED_Off(i);
	}
}

void ledAllOn(){
	int i;
	for(i=0; i<8; i++){
		LED_On(i);
	}
}

//main menu
void mainMenu(){
	selected = KBD_DOWN;	//to start musicSel 
	while(1){
		if(lastPageSel != menuSel){
			mainMenuSelNew = mainMenuSel;
		}
		lastPageSel = menuSel;
		if(selected == KBD_UP){
			mainMenuSelNew--;
		}
		else if(selected == KBD_DOWN){
		mainMenuSelNew++;
		}
		else if(selected == KBD_SELECT){
			ledAllOff();
			if(mainMenuSel == musicSel){
				//play music
				music();
			}
			else if(mainMenuSel == imagesSel){
				//display imgs
				images();
			}
			else if(mainMenuSel == gameSel){
				//play game
				game();
			}
		}
		
		if(mainMenuSelNew > gameSel){
			mainMenuSelNew = musicSel;
		}
		else if(mainMenuSelNew < musicSel){
			mainMenuSelNew = gameSel;
		}
		
		if(lastPageSel == menuSel && mainMenuSel == mainMenuSelNew){ break; }
		
		mainMenuSel = mainMenuSelNew;
		if(lastPageSel != menuSel){
			GLCD_Clear(Black);                         /* Clear graphical LCD display   */
			GLCD_SetBackColor(Black);
			GLCD_SetTextColor(Green);
			GLCD_DisplayString(0, 0, __FI, mediaPlayerTitle);
		}
		
		if(mainMenuSel == musicSel){
			GLCD_SetBackColor(Green);
			GLCD_SetTextColor(Black);
			GLCD_DisplayString(3, 0, __FI, musicTitle);
			GLCD_SetBackColor(Black);
			GLCD_SetTextColor(Green);
			GLCD_DisplayString(4, 0, __FI, imagesTitle);
			GLCD_DisplayString(5, 0, __FI, gameTitle);
		}
		else if(mainMenuSel == imagesSel){
			GLCD_SetBackColor(Black);
			GLCD_SetTextColor(Green);
			GLCD_DisplayString(3, 0, __FI, musicTitle);
			GLCD_SetBackColor(Green);
			GLCD_SetTextColor(Black);
			GLCD_DisplayString(4, 0, __FI, imagesTitle);
			GLCD_SetBackColor(Black);
			GLCD_SetTextColor(Green);
			GLCD_DisplayString(5, 0, __FI, gameTitle);
		}
		else if(mainMenuSel == gameSel){
			GLCD_SetBackColor(Black);
			GLCD_SetTextColor(Green);
			GLCD_DisplayString(3, 0, __FI, musicTitle);
			GLCD_DisplayString(4, 0, __FI, imagesTitle);
			GLCD_SetBackColor(Green);
			GLCD_SetTextColor(Black);
			GLCD_DisplayString(5, 0, __FI, gameTitle);
		}
		ledAllOff();
		LED_On(mainMenuSel-1);
		delay(500);
		
		while (get_button() == 0){}
		selected = get_button();		
	}
}

void music(){
	lastPageSel = musicSel;
	
	GLCD_Clear(Black);                         /* Clear graphical LCD display   */
	GLCD_SetBackColor(Black);
	GLCD_SetTextColor(Green);
	GLCD_DisplayString(0, 0, __FI, mediaPlayerTitle);
	GLCD_DisplayString(3, 0, __FI, "   Streaming Music  ");

	streamAudio();
}

void images(){
	int totalPics = 3 - 1;
	int imgSel = totalPics + 1;
	delay(800);
	
	lastPageSel = imagesSel;
	
	while(1){
		while(get_button() == 0 && imgSel != (totalPics+1) ){}
		if(get_button() == KBD_RIGHT){
			imgSel++;
		}
		else if(get_button() == KBD_LEFT){
			imgSel--;
		}
		else if(get_button() == KBD_SELECT){
			break;
		}

		//fix out of bound
		if(imgSel < 0){
			imgSel = totalPics;
		}
		else if(imgSel > totalPics){
			imgSel = 0;
		}
		//display
		if(imgSel == 0){
			GLCD_Bitmap(0, 0, 320, 240, gotPic);
		}
		else if(imgSel == 1){
			GLCD_Bitmap(0, 0, 320, 240, minionsPic);
		}
		else if(imgSel == 2){
			GLCD_Bitmap(0, 0, 320, 240, greenPic);
		}
		ledAllOff();
		LED_On(imgSel);
		
	} //end of loop
	ledAllOff();
}

void game(){
	int i;
	int r2W = 50;
	int r2H = 80+2+2;
	int r2Y = 0;
	int r2YMax = 240-r2H;
	int r2Speed = 2;
	int lazerW = 28+2;
	int lazerH = 10;
	int lazerSpeed = 2;
	int lazerSpeedSlow = 2;
	int lazer1X = 0;
	int lazer2X = 0;
	int lazer3X = 0;
	int lazer1Y = 0;
	int lazer2Y = 0;
	int lazer3Y = 0;
	int score = -150;
	char scoreStr[20];
	lastPageSel = gameSel;
	ledAllOff();
	
	//game screen
	GLCD_Clear(Black);                         /* Clear graphical LCD display   */
	GLCD_SetBackColor(Black);
	GLCD_SetTextColor(Green);
	GLCD_DisplayString(1, 0, __FI, "     Lazer Dodge    ");
	GLCD_DisplayString(3, 0, __FI, "  Help R2-D2 dodge  ");
	GLCD_DisplayString(4, 0, __FI, "the lazers pressing ");
	GLCD_DisplayString(5, 0, __FI, "  up and down, or   ");
	GLCD_DisplayString(6, 0, __FI, "   press left or    ");
	GLCD_DisplayString(7, 0, __FI, "   right to exit.   ");
	delay(800);
	//take input
	while(get_button() == 0){}
	if(get_button() == KBD_UP || get_button() == KBD_DOWN || get_button() == KBD_SELECT){
		
	}
	else{
		//return to main menu
		GLCD_Clear(Black);                         /* Clear graphical LCD display   */
		GLCD_SetBackColor(Black);
		GLCD_SetTextColor(Green);
		GLCD_DisplayString(0, 0, __FI, mediaPlayerTitle);
		return;
	}
	
	//start game
	GLCD_Clear(White);
	lazer1Y = r2Y + r2H/2 - 20;
	lazer2Y = 240;
	lazer3Y = 240;
	i = 0;
	lazer1X = -100;
	lazer2X = 10000;
	lazer3X = 10000;	
	while(1){
		if(i==107){
			lazer2X = -100;
		}
		if(i==214){
			lazer3X = -100;
		}
		
		if(lazer1X+lazerW < 0){
			lazer1X = 320-lazerW/2;
			lazer1Y = r2Y + r2H/2 - 20;
			score+=50;
		}
		if(lazer2X+lazerW < 0){
			lazer2X = 320-lazerW/2;
			lazer2Y = r2Y + r2H/2 - 20;
			score+=50;
		}
		if(lazer3X+lazerW < 0){
			lazer3X = 320-lazerW/2;
			lazer3Y = r2Y + r2H/2 - 20;
			score+=50;
		}
		if(i%10 == 0){
			score++;
		}

		//draw pics
		lazer1X -= lazerSpeed;
		lazer2X -= lazerSpeed;
		lazer3X -= lazerSpeedSlow;
		GLCD_Bitmap(0, r2Y, r2W, r2H, r2Pic);
		GLCD_Bitmap(lazer1X, lazer1Y, lazerW, lazerH, lazerPic);
		GLCD_Bitmap(lazer2X, lazer2Y, lazerW, lazerH, lazerPic);
		GLCD_Bitmap(lazer3X, lazer3Y, lazerW, lazerH, lazerPic);
		
		//check if hit
		//lazer 1
		if(lazer1X < r2W){
			if(lazer1Y+3 > r2Y && (lazer1Y+lazerH) < (r2Y+r2H)){
				//game over
				break;
			}				
		}
		else if(lazer2X < r2W){
			if(lazer2Y+3 > r2Y && (lazer2Y+lazerH) < (r2Y+r2H)){
				//game over
				break;
			}				
		}
		else if(lazer3X < r2W){
			if(lazer3Y+3 > r2Y && (lazer3Y+lazerH) < (r2Y+r2H)){
				//game over
				break;
			}				
		}
		
		//move r2
		if(get_button() == KBD_DOWN){
			r2Y+=r2Speed;
		}else if(get_button() == KBD_UP){
			r2Y-=r2Speed;
		}
		if(r2Y < 0){
			r2Y = 0;
		}else if(r2Y > r2YMax){
			r2Y = r2YMax;
		}
		
		i++;
	}
	
	//game over
	GLCD_Clear(Black);                         /* Clear graphical LCD display   */
  GLCD_Bitmap(105, 75, 95, 76, gameOverPic);
	GLCD_SetBackColor(Black);
  GLCD_SetTextColor(Green);
	if(score<0) { score = 0;}
	sprintf(scoreStr, " Your scored:%6d    ",  score);
	GLCD_DisplayString(8, 0, __FI, (unsigned char*) scoreStr);
	delay(800);
	
	//take input
	while(get_button() == 0){}
	game();
	
}

int main (void) {
	LED_Init();
	
	#ifdef __USE_LCD
  GLCD_Init();                               /* Initialize graphical LCD (if enabled */

  GLCD_Clear(Black);                         /* Clear graphical LCD display   */
  GLCD_SetBackColor(Black);
  GLCD_SetTextColor(Green);
  GLCD_DisplayString(0, 0, __FI, mediaPlayerTitle);
	GLCD_SetBackColor(Green);
	GLCD_SetTextColor(Black);
  GLCD_DisplayString(3, 0, __FI, musicTitle);
	GLCD_SetBackColor(Black);
	GLCD_SetTextColor(Green);
  GLCD_DisplayString(4, 0, __FI, imagesTitle);
	GLCD_DisplayString(5, 0, __FI, gameTitle);
#endif
	
	mainMenu();              						
	
}

