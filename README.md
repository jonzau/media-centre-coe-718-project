# README #

This is a project for COE718 (Embedded Systems) - Fall/2015. The project consists of a Media Centre capable of playing audio, displaying images, and running and interactive graphic game.
The project was implemented to run on a MXB1768 board with an ARM Cortex M3 processor using C and the Keil uVision environment.

Author: Ionathan Zauritz - izauritz@ryerson.ca